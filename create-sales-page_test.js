Feature('create-sales-page');

const userObject = { name: 'ekaterina', password: '85a2V65**NRGG*Lv'}
const salesLandingObject = { href: "#/landing-pages/SALES/theme/4", name: 'New Sales Page'};
const generateUniqueName = (name) => {
    const randomNumber = Math.floor(Math.random() * Math.floor(1000));
    return `${name}${randomNumber}`
}

Scenario('sales page can be created sucessfully', (I) => {
    const salesLandingName = generateUniqueName(salesLandingObject.name);
    I.login(userObject.name, userObject.password);

    // TODO: Can be replaced with url visit
    I.click({id: 'nav-marketing-pages-button'});
    I.click({css: '.header-action'}); // 'Add'

    I.click('Sales'); // TODO: Language-dependent, not good
    I.waitForClickable({css: '.overview-tile'}, 5);
    I.retry({ retries: 5, maxTimeout: 5000 }).click({css: `[href="${salesLandingObject.href}"]`});
    I.click(locate('.btn').withText('Use this template'));

    I.fillField({id: 'title'}, salesLandingName);
    I.click({css: '[type=submit]'});

    I.waitForClickable({id: 'exit-button'}, 2);
    I.retry({ retries: 5, maxTimeout: 5000 }).click({id: 'exit-button'});

    I.see(salesLandingName); // TODO: only one!
});
