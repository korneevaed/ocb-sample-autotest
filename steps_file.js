// in this file you can append custom step methods to 'I' object

module.exports = function() {
  return actor({
    login: function(username, password) {
      this.amOnPage('de/login');
      this.fillField({name: '_username'}, username);
      this.fillField({name: '_password'}, password);
      this.click({name: '_submit'});
    }
  });
}